import { createStore } from 'vuex'
// import game from './modules/game'

const store = createStore({
    state: () => ({
        previousGameBoardObj: [],
    }),

    getters: {
        //
    },

    mutations: {
        resetGameObj (state) {
            state.previousGameBoardObj = []
        },
        pushObject (state, obj) {
            state.previousGameBoardObj[state.previousGameBoardObj.length] = obj
        },
        delleteObj (state) {
            state.previousGameBoardObj.length =
                state.previousGameBoardObj.length - 1
        },
    },

    actions: {
        //
    },
    // modules: {
    //     game,
    // },
})

export default store
